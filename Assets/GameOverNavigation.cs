﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameOverNavigation : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Restart(){
		string name = SceneManager.GetActiveScene ().name;
		SceneManager.UnloadScene(name);
		SceneManager.LoadScene ("MainMenu");
		SceneManager.UnloadScene("MainMenu");
		SceneManager.LoadScene(name);
	}

	public void GoToMain(){
		SceneManager.UnloadScene(name);
		SceneManager.LoadScene ("MainMenu");
	}

}
